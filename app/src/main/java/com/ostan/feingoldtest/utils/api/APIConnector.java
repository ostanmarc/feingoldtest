package com.ostan.feingoldtest.utils.api;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.pojo.SingleUser;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.ostan.feingoldtest.utils.api.APIConstants.BASE_URL;

/**
 * Created by marco on 17/12/2016.
 */

public class APIConnector {

    public void getAllUsersReactive(int since, int perPage, Observer<List<ListItemUser>> observer) {
        APIInterface testService = getReactiveAPIInterfaceInstance(BASE_URL);
        testService.getAllUsersReactive(since, perPage)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(observer);
    }

    public void loadUserData(String userName, Observer<SingleUser> observer){
        getReactiveAPIInterfaceInstance(BASE_URL)
                .getSingleReactive(userName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private APIInterface getReactiveAPIInterfaceInstance(String base_url) {

        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();

        APIInterface protocolInstance = retrofit.create(APIInterface.class);

        return protocolInstance;
    }

    private APIInterface getAPIInterfaceInstance(String baseUrlForRequest) {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .build();

                Request.Builder requestBuilder = original.newBuilder()
                        .url(url);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrlForRequest)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIInterface protocolInstance = retrofit.create(APIInterface.class);

        return protocolInstance;
    }



}
