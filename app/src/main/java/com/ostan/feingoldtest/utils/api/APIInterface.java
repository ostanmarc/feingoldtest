package com.ostan.feingoldtest.utils.api;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.pojo.SingleUser;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

import static com.ostan.feingoldtest.utils.api.APIConstants.PATH;

public interface APIInterface {

    @GET(PATH)
    public Observable<List<ListItemUser>> getAllUsersReactive(@Query(APIConstants.GET_USERS_QUERRY_PARAM_SINCE) int since,
                                                              @Query(APIConstants.GET_USERS_QUERRY_PARAM_PER_PAGE) int perPage);

    @GET(PATH+"/{"+APIConstants.GET_SINGLE_USER_QUERY_PARAM_USERNAME+"}")
    public Observable<SingleUser> getSingleReactive(@Path(APIConstants.GET_SINGLE_USER_QUERY_PARAM_USERNAME) String username);
}
