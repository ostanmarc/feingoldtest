package com.ostan.feingoldtest.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by marco on 28/12/2016.
 */

public class TableGenerator {

    int textColor = Color.BLACK;
    HashMap<String, String> data = null;
    final Context ctx;

    public TableGenerator setTextSize(float textSize) {
        this.textSize = textSize;
        return this;
    }

    float textSize = 15f;


    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

    public void setLayoutParams(LinearLayout.LayoutParams layoutParams) {
        this.layoutParams = layoutParams;
    }

    public TableGenerator(Context ctx) {
        this.ctx = ctx;
    }

    public TableGenerator setTextsColor(int color) {
        textColor = color;
        return this;
    }

    public TableGenerator setData(HashMap<String, String> data) {
        this.data = data;
        return this;
    }

    public LinearLayout generate() {
        if (data == null || data.isEmpty()) {
            return null;
        }
        LinearLayout result = new LinearLayout(ctx);
        result.setLayoutParams(layoutParams);
        result.setOrientation(LinearLayout.VERTICAL);
        result.setGravity(Gravity.CENTER_VERTICAL);

        for (String iterator : data.keySet()) {
            String src = data.get(iterator);

            // Verify we work with a valid and presentable data
            if(src == null || src.isEmpty()) {
                continue;
            }
            result.addView(generateTextView(ctx, iterator, src));
        }

        return result;
    }

    private TextView generateTextView(Context ctx, String title, String data) {

        TextView result = new TextView(ctx);
        result.setTextColor(textColor);
        result.setText(title + " : " + data);
        result.setGravity(Gravity.CENTER_VERTICAL);
        result.setTextSize(textSize);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f);
        result.setLayoutParams(params);

        return result;
    }


}
