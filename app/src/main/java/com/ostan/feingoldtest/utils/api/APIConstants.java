package com.ostan.feingoldtest.utils.api;

/**
 * Created by marco on 29/12/2016.
 */

public class APIConstants {
    public static final String BASE_URL = "https://api.github.com/";
    public static final String GET_USERS_QUERRY_PARAM_SINCE = "since";
    public static final String GET_USERS_QUERRY_PARAM_PER_PAGE = "per_page";

    public static final String GET_SINGLE_USER_QUERY_PARAM_USERNAME = "username";
    public static final String PATH = "/users";
}
