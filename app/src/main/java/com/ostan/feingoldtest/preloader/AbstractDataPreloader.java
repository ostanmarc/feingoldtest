package com.ostan.feingoldtest.preloader;

/**
 * Class that receives data of card that is being drawn from the
 * list and decides when to load more items so the scrolling will be smooth
 */

public abstract class AbstractDataPreloader {


    public void notifyItemRendered(int itemIndex, int itemsCount) {

        // Check if we already reached the trigger point
        if(((float)itemIndex / (float)itemsCount) > getPercentageTriggerForLoadingTrigger()){

            // request additional load
            callForRequestPreload(itemsCount, (int)(itemsCount * getPercentageSizeOfLoadRequest()));

        }

    }

    public abstract void callForRequestPreload(int startShift, int numberOfItemsRequested);

    /**
     * Configurable by implementers.
     * Precentage from list that is already presented in order to start loading
     * of the addidtional items
     * */
    protected abstract float getPercentageTriggerForLoadingTrigger();

    /**
     * Configurable by implementers.
     * Percent formula of the number of the items to request, of the currently presented items
     * Could also be exchanged by more complicated logic that will take this desicion base on scrolling speed.
     * */
    protected abstract float getPercentageSizeOfLoadRequest();
}
