package com.ostan.feingoldtest.preloader;

import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModel;

/**
 * Created by marco on 26/12/2016.
 */

public class UsersListDataPreloader extends AbstractDataPreloader {

    final IUsersListModel model;

    private final float PRELOADING_TRIGGER_PERCENTAGE = 0.8f;
    private final float PRELOADING_REQUEST_PERCENTAGE = 1.0f;

    private float internalTriggerPercentage = -1;
    private float internalRequestPercentage = -1;

    public UsersListDataPreloader(IUsersListModel model) {
        this.model = model;
        this.internalRequestPercentage = PRELOADING_REQUEST_PERCENTAGE;
        this.internalTriggerPercentage = PRELOADING_TRIGGER_PERCENTAGE;
    }

    public UsersListDataPreloader(IUsersListModel model, float internalTriggerPercentage, float internalRequestPercentage) {
        this.model = model;
        this.internalTriggerPercentage = internalTriggerPercentage;
        this.internalRequestPercentage = internalRequestPercentage;
    }

    @Override
    public void callForRequestPreload(int startShift, int numberOfItemsRequested) {
        // Perform call to load required amount of items
        model.onLoadItemsRequested(startShift, numberOfItemsRequested);
    }

    @Override
    protected float getPercentageTriggerForLoadingTrigger() {
        return internalTriggerPercentage;
    }

    @Override
    protected float getPercentageSizeOfLoadRequest() {
        return internalRequestPercentage;
    }
}
