package com.ostan.feingoldtest.screens.userdetailscreen;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.ostan.feingoldtest.R;
import com.ostan.feingoldtest.pojo.SingleUser;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailPresenter;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailView;
import com.ostan.feingoldtest.utils.TableGenerator;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by marco on 28/12/2016.
 */

public class UserDetailViewImpl implements IUserDetailView {

    final IUserDetailPresenter presenter;
    final AppCompatActivity activity;

    @BindView(R.id.details_container)
    LinearLayout container;

    @BindView(R.id.app_bar)
    AppBarLayout appBar;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    @BindView(R.id.user_detail_icon)
    ImageView userIcon;

    @BindView(R.id.detail_toolbar)
    Toolbar toolbar;

    @BindView(R.id.fading_ui_container)
    RelativeLayout fadindUiContainer;

    @BindView(R.id.user_data_holder)
    LinearLayout headerDataHolder;

    @BindView(R.id.progress_view)
    CircularProgressView progressView;



    public UserDetailViewImpl(IUserDetailPresenter presenter, AppCompatActivity activity) {
        this.presenter = presenter;
        this.activity = activity;
        ButterKnife.bind(this, activity);
        setupViews();
    }

    @Override
    public void setIsLoading(boolean isLoading) {
        progressView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setUserData(SingleUser user) {

        loadUserIcon(user.getAvatarUrl());

        HashMap<String, String> dataSrc = new HashMap<>();
        dataSrc.put(activity.getString(R.string.user_detail_followers_prefix), user.getFollowers()+"");
        dataSrc.put(activity.getString(R.string.user_detail_public_repos_prefix), user.getPublicRepos()+"");
        dataSrc.put(activity.getString(R.string.user_detail_location_prefix), user.getLocation());

        headerDataHolder.addView(new TableGenerator(activity)
                .setData(dataSrc)
                .setTextsColor(activity.getResources().getColor(R.color.color_user_detail_screen_text_header))
                .generate());


        dataSrc = new HashMap<>();

        dataSrc.put(activity.getString(R.string.user_detail_name_prefix),user.getName());
        dataSrc.put(activity.getString(R.string.user_detail_followers_prefix), user.getBio());
        dataSrc.put(activity.getString(R.string.user_detail_public_repos_prefix), user.getBlog());
        dataSrc.put(activity.getString(R.string.user_detail_location_prefix), user.getCompany());
        dataSrc.put(activity.getString(R.string.user_detail_email_prefix), user.getEmail());
        dataSrc.put(activity.getString(R.string.user_detail_hireable_prefix), user.getHireable() ? "yes" : "no");
        dataSrc.put(activity.getString(R.string.user_detail_subscriptions_url_prefix), user.getSubscriptionsUrl());
        dataSrc.put(activity.getString(R.string.user_detail_starred_url_prefix), user.getStarredUrl());


        container.addView(new TableGenerator(activity)
                .setData(dataSrc)
                .setTextSize(18)
                .generate());

    }

    public void loadUserIcon(String iconUrl) {
        Glide.with(activity).load(iconUrl).diskCacheStrategy(DiskCacheStrategy.ALL).into(userIcon);
    }

    @Override
    public void notifyUserInputIsInvalid() {
        Snackbar.make(toolbar, "Something went wrong, try to re-enter the screen", Snackbar.LENGTH_INDEFINITE).show();
    }


    private void setupViews() {
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
                fadindUiContainer.setAlpha(0.98f - percentage);
            }



        });

    }
}