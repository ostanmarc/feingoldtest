package com.ostan.feingoldtest.screens.userdetailscreen;

import android.support.v7.app.AppCompatActivity;

import com.ostan.feingoldtest.pojo.SingleUser;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailModel;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailModelCallback;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailPresenter;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailView;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailsActivityCallback;

/**
 * Created by marco on 28/12/2016.
 */

public class UserDetailPresenterImpl implements IUserDetailPresenter, IUserDetailModelCallback, IUserDetailsActivityCallback {

    final IUserDetailView view;
    final IUserDetailModel model;
    final AppCompatActivity activity;

    public UserDetailPresenterImpl(AppCompatActivity activity, IUserDetailView view, IUserDetailModel model) {
        this.activity = activity;
        this.view = view;
        this.model = model;
    }

    public UserDetailPresenterImpl(AppCompatActivity activity) {
        this.activity = activity;
        this.model = new UserDetailModelImpl(this);
        this.view = new UserDetailViewImpl(this, activity);

    }

    @Override
    public void onUserDataArrived(SingleUser userData) {
        if(userData == null) {
            view.notifyUserInputIsInvalid();
            return;
        }
        view.setIsLoading(false);
        view.setUserData(userData);
    }

    @Override
    public void onUserNameReceived(String username) {

        if(!isInputValid(username)) {
            view.notifyUserInputIsInvalid();
            return;
        }
        view.setIsLoading(true);
        model.requestUserDataLoading(username);
    }


    private boolean isInputValid(String input) {

        if (input == null) {
            return false;
        }
        if (input.isEmpty()) {
            return false;
        }

        return true;

    }
}
