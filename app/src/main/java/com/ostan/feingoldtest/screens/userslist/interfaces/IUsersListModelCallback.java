package com.ostan.feingoldtest.screens.userslist.interfaces;

import com.ostan.feingoldtest.pojo.ListItemUser;

import java.util.List;

/**
 * Created by marco on 26/12/2016.
 */

public interface IUsersListModelCallback {
    /**
     * Notification that new loaded items list arrived
     * @param - arrived data
     * */
    public void onNewItemsArrived(List<ListItemUser> item);

    /**
     * Layout retore trigger that is called when we have data that was saved before the screen rotation
     * @param isGrid - saved state
     * */
    public void onLayoutRestore(boolean isGrid);

    /**
     * Called when model experiences some network related issues
     * @param isHappening - ussues are happening at the time of the call
     * */
    public void onNetworkIssueOccured(boolean isHappening);

    /**
     * Called when model is working on loading additional items into list
     * @param isHappening - loading proccess is happening at the time of the call
     * */
    public void onLoadingInProgress(boolean isHappening);
}
