package com.ostan.feingoldtest.screens.userdetailscreen.interfaces;

/**
 * Created by marco on 28/12/2016.
 */

public interface IUserDetailPresenter {
    /**Currently no functionality has been added here since no interaction from the view to presenter is required
     * #Placeholder for future addings
     * */
}
