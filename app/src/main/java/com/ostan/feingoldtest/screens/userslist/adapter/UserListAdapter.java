package com.ostan.feingoldtest.screens.userslist.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ostan.feingoldtest.R;
import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.screens.userslist.interfaces.IAdapterEventsListener;

import java.util.List;

/**
 * Adapter class that presents items that arrive in a Recycler view
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private final List<ListItemUser> mValues;
    private final Context context;
    private final IAdapterEventsListener viewImpl;

    public UserListAdapter(List<ListItemUser> items, Context context, IAdapterEventsListener view) {
        mValues = items;
        this.context = context;
        this.viewImpl = view;
    }

    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_list_content, parent, false);
        return new UserListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final UserListAdapter.ViewHolder holder, final int position) {

        int correctedPosition = position % mValues.size();
        holder.mItem = mValues.get(correctedPosition);
        holder.userNickname.setText(holder.mItem.getLogin());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImpl.onItemClicked(holder.mItem.getLogin());
            }
        });

        // Load the user's image
        Glide.with(context)
                .load(holder.mItem.getAvatarUrl())
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .placeholder(R.drawable.ic_camera_black_24dp)
                .into(holder.userAvatar);

        // Report item drawing to the rellevant component (UserListDataPreloader)
        viewImpl.itemRendered(correctedPosition, mValues.size());
    }

    @Override
    public int getItemCount() {
        // Here we intervent the adapters logics regarding the number of items
        return mValues.size() > 0 ? Integer.MAX_VALUE : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView userNickname;
        public final ImageView userAvatar;
        public ListItemUser mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            userNickname = (TextView) view.findViewById(R.id.user_nickname);
            userAvatar = (ImageView) view.findViewById(R.id.user_avatar_iv);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + userNickname.getText() + "'";
        }
    }

    public void insertUsers(List<ListItemUser> items) {
// TODO find a better way to handle the new items request and duplications in the server's responses
//        for (ListItemUser iterator : mValues) {
//            if(items.remove(iterator)) {
//
//            }
//        }
        mValues.addAll(items);
        notifyDataSetChanged();
    }

}

