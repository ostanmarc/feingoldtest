package com.ostan.feingoldtest.screens.userslist.interfaces;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Interface to activity that handles the UserListScreen
 */

public interface IActivityInterface {
    public void startActivity(Intent intent);
    public AppCompatActivity getActivity();
}
