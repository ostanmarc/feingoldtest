package com.ostan.feingoldtest.screens.userslist;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.ostan.feingoldtest.R;
import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.screens.userslist.adapter.UserListAdapter;
import com.ostan.feingoldtest.screens.userslist.interfaces.IAdapterEventsListener;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListPresenter;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * View logics implementation class
 * Responsible for all the user's visible functionality of this screen
 */

public class UsersListViewImpl implements IUsersListView, IAdapterEventsListener {


    private final IUsersListPresenter presenter;

    @BindView(R.id.user_list)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @OnClick(R.id.fab)
    public void notifyPresenter() {
        presenter.onLayoutTypeTriggered();
    }

    @BindView(R.id.network_error_notification)
    ImageView errorNotification;

    @BindView(R.id.user_list_progress_view)
    CircularProgressView progressView;

    private UserListAdapter adapter;

    private AppCompatActivity activity;

    public UsersListViewImpl(IUsersListPresenter presenter, AppCompatActivity activity) {
        this.presenter = presenter;
        this.activity = activity;
        ButterKnife.bind(this, this.activity);
        assert recyclerView != null;
        setupRecyclerView(recyclerView);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        adapter = new UserListAdapter(new ArrayList<ListItemUser>(), activity.getApplicationContext(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void insertNewItems(List<ListItemUser> items) {
        adapter.insertUsers(items);
    }


    @Override
    public void changeLayout(boolean isGrid) {

        if (isGrid) {
            recyclerView.setLayoutManager(new GridLayoutManager(activity, 3, 1, false));
            fab.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_list_24dp));
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
            fab.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_grid_24dp));
        }

    }

    @Override
    public void notifyNetworkIssue(boolean isHappening) {
        errorNotification.setVisibility(isHappening ? View.VISIBLE : View.GONE);
    }

    @Override
    public void notifyLoadingIsInProgress(boolean isHappening) {
        progressView.setVisibility(isHappening ? View.VISIBLE : View.GONE);
    }

    @Override
    public void itemRendered(int position, int count) {
        presenter.onNewItemRendered(position, count);
    }

    @Override
    public void onItemClicked(String username) {
        presenter.onUserEntryClicked(username);
    }
}
