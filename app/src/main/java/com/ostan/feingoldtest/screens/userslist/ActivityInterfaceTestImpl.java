package com.ostan.feingoldtest.screens.userslist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.ostan.feingoldtest.screens.userslist.interfaces.IActivityInterface;

/**
 * Created by marco on 29/12/2016.
 */

public class ActivityInterfaceTestImpl implements IActivityInterface {

    @Override
    public void startActivity(Intent intent) {

    }

    @Override
    public AppCompatActivity getActivity() {
        return null;
    }
}
