package com.ostan.feingoldtest.screens.userdetailscreen.interfaces;

import com.ostan.feingoldtest.pojo.SingleUser;

/**
 * Created by marco on 28/12/2016.
 */

public interface IUserDetailView {
    /**
     * present to the user that userdata is loading
     * @param isLoading - is loading now
     * */
    public void setIsLoading(boolean isLoading);

    /**
     * Sets user data arrived from outside and calls the view to present this data to the user
     * @param  user - the data to be presented
     * */
    public void setUserData(SingleUser user);

    /**
     * Notify user in case screen receivde no data or invalid data
     * */
    public void notifyUserInputIsInvalid();
}
