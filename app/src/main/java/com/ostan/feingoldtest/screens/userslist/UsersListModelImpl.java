package com.ostan.feingoldtest.screens.userslist;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModel;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModelCallback;
import com.ostan.feingoldtest.utils.api.APIConnector;

import java.util.List;

import rx.Observer;

/**
 * Created by marco on 26/12/2016.
 */

public class UsersListModelImpl implements IUsersListModel, Observer<List<ListItemUser>> {

    final IUsersListModelCallback callback;
    DataFragment dataFragment;
    final Activity activity;
    final APIConnector conector;

    private final int START_USERS_NUMNER = 60;

    public UsersListModelImpl(IUsersListModelCallback callback, AppCompatActivity activity) {
        this.callback = callback;
        this.activity = activity;
        conector = new APIConnector();

        createOrRestoreDataFragment(activity);
        restoreDataIfNeeded();

    }

    public UsersListModelImpl(IUsersListModelCallback callback, AppCompatActivity activity, DataFragment dataFragment, APIConnector apiConnector) {
        this.callback = callback;
        this.dataFragment = dataFragment;
        this.activity = activity;
        this.conector = apiConnector;
    }

    // Flag prevents parallel calls to load additional users
    boolean isRequestRunning = false;

    @Override
    public void onLoadItemsRequested(int since, int itemsNumber) {
        if (isRequestRunning) {
            return;
        }
        callback.onLoadingInProgress(true);
        isRequestRunning = true;
        callback.onNetworkIssueOccured(false);
        conector.getAllUsersReactive(since, itemsNumber, this);
        //TODO add handling for the screen rotation case while loading
        // todo items by making the data fragment the observer that is passed to the AIPConnector
    }

    @Override
    public void saveLayoutState(boolean isGrid) {
        dataFragment.setGrid(isGrid);
    }

    private void createOrRestoreDataFragment(AppCompatActivity activity) {

        dataFragment = (DataFragment) activity.getSupportFragmentManager().findFragmentByTag(DataFragment.TAG);
        if (dataFragment == null) {
            dataFragment = DataFragment.getInstance();
            activity.getSupportFragmentManager().beginTransaction().add(dataFragment, DataFragment.TAG).commit();
        }
    }

    @Override
    public void onCompleted() {
        callback.onLoadingInProgress(false);
    }

    @Override
    public void onError(Throwable e) {
        isRequestRunning = false;
        callback.onNetworkIssueOccured(true);
        callback.onLoadingInProgress(false);
        Log.i("LOG","Exception on network call");
        if(e != null) {
            e.printStackTrace();
        }

    }

    @Override
    public void onNext(List<ListItemUser> listItemUser) {


        if(listItemUser == null || listItemUser.isEmpty()) {
            callback.onNetworkIssueOccured(true);
            Log.i("LOG","Empty or null esponse");
            return;
        }

        dataFragment.addItems(listItemUser);
        callback.onNewItemsArrived(listItemUser);

        isRequestRunning = false;
    }

    private void restoreDataIfNeeded() {
//         First creation of the screen
        if (dataFragment.getAllItems().isEmpty()) {
            onLoadItemsRequested(0, START_USERS_NUMNER);
        } else {
//         Screen was already existing and now we just need to restore data after screen rotation
            callback.onLayoutRestore(dataFragment.isGrid());
            callback.onNewItemsArrived(dataFragment.getAllItems());
        }

    }

}
