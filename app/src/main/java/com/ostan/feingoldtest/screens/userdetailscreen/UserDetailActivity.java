package com.ostan.feingoldtest.screens.userdetailscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.ostan.feingoldtest.R;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailsActivityCallback;
import com.ostan.feingoldtest.screens.userslist.UserListActivity;


public class UserDetailActivity extends AppCompatActivity {

    public static final String ARG_USERNAME = "username";

    IUserDetailsActivityCallback callback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        setupViews(savedInstanceState);
        callback = new UserDetailPresenterImpl(this);
        callback.onUserNameReceived(getIntent().getExtras().getString(ARG_USERNAME));

    }


    private void setupViews(Bundle savedInstanceState){

        if (savedInstanceState == null) {
            UserDetailFragment fragment = new UserDetailFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.user_detail_container, fragment, UserDetailFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button. In the case of this
            // activity, the Up button is shown. Use NavUtils to allow users
            // to navigate up one level in the application structure. For
            // more details, see the Navigation pattern on Android Design:
            //
            // http://developer.android.com/design/patterns/navigation.html#up-vs-back
            //
            NavUtils.navigateUpTo(this, new Intent(this, UserListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
