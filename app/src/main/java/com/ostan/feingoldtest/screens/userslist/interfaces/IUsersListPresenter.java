package com.ostan.feingoldtest.screens.userslist.interfaces;

/**
 * Created by marco on 26/12/2016.
 */

public interface IUsersListPresenter {

    /**
     * Notify presenter that item in the list has been clicked
     * @param  username - username of the user that has been clicked
     * */
    public void onUserEntryClicked(String username);

    /**
     * Layout control has been triggered
     * */
    public void onLayoutTypeTriggered();

    /**
     * notification arriving from the view regarding the currently drawn item
     * should be passed to the component that decides  when to send request for the new items
     * @param position - currently drawn item's position
     * @param count - currently loaded items number
     * */
    public void onNewItemRendered(int position, int count);
}
