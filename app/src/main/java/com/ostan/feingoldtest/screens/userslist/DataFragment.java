package com.ostan.feingoldtest.screens.userslist;

import android.support.v4.app.Fragment;

import com.ostan.feingoldtest.pojo.ListItemUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that hods all the loaded data and no UI in
 * order to save the data from being destroyed when screen is rotated.
 */

public class DataFragment extends Fragment {

    public static final String TAG = "dataFRagment";


    private List<ListItemUser> usersFromGitHub = new ArrayList<ListItemUser>();

    private boolean isGrid = false;

    public static DataFragment getInstance(){
        DataFragment result = new DataFragment();
        result.setRetainInstance(true);
        return result;
    }

    public void addItems(List<ListItemUser> users){
        usersFromGitHub.addAll(users);
    }

    public List<ListItemUser> getAllItems(){
        return usersFromGitHub;
    }


    public boolean isGrid() {
        return isGrid;
    }

    public void setGrid(boolean grid) {
        isGrid = grid;
    }

}
