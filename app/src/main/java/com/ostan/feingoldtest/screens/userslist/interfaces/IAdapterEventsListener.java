package com.ostan.feingoldtest.screens.userslist.interfaces;

/**
 * Created by marco on 28/12/2016.
 */

public interface IAdapterEventsListener {

    /**
     * Report from adapter that item was drawn used for the preloading additional items logic
     * @param position - currently drawn item
     * @param count currently existing items
     * */
    public void itemRendered(int position, int count);

    /**
     * Report to the adapterEvents listener which item was clicked
     * @param username - item's unique name that will be used by listener or others in order to respond
     * properly
     * */
    public void onItemClicked(String username);

}
