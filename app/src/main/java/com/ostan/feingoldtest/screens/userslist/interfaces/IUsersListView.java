package com.ostan.feingoldtest.screens.userslist.interfaces;

import com.ostan.feingoldtest.pojo.ListItemUser;

import java.util.List;

/**
 * Created by marco on 26/12/2016.
 */

public interface IUsersListView {

    /**
     * Add new Items loaded in background
     * @param items - newly arrived data
     * */
    public void insertNewItems(List<ListItemUser> items);

    /**
     * Change layout on the user visible level
     * @param  isGrid - if true layout will be grid, otherwise list
     * */
    public void changeLayout(boolean isGrid);

    /**
     * Notify user that he has a network related issue.
     * @param isHappening - is it happening at the moment of the call?
     * */
    public void notifyNetworkIssue(boolean isHappening);

    /**
     * Notify user that we are loading more users
     * @param isHappening - is it happening at the moment of the call?
     * */
    public void notifyLoadingIsInProgress(boolean isHappening);

}
