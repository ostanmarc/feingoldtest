package com.ostan.feingoldtest.screens.userdetailscreen.interfaces;

/**
 * Created by marco on 28/12/2016.
 */

public interface IUserDetailsActivityCallback {

    /**
     * Called when username received and should be used
     * for data presentation through the proccess of user loading
     * @param username*/
    public void onUserNameReceived(String username);
}
