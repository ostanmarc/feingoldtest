package com.ostan.feingoldtest.screens.userslist.interfaces;

/**
 * Created by marco on 26/12/2016.
 */

public interface IUsersListModel {
    /**
     * Called when new items should be loaded
     * @param itemsNumber - number of users to be loaded in comming request
     * */
    public void onLoadItemsRequested(int since, int itemsNumber);

    /**
     * Save layout state for case that screen will be rotated and all the views recreated
     * @param  isGrid - current layout state
     * */
    public void saveLayoutState(boolean isGrid);
}
