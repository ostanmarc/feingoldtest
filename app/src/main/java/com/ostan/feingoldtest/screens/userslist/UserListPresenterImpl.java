package com.ostan.feingoldtest.screens.userslist;

import android.content.Intent;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.preloader.UsersListDataPreloader;
import com.ostan.feingoldtest.screens.userdetailscreen.UserDetailActivity;
import com.ostan.feingoldtest.screens.userslist.interfaces.IActivityInterface;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModel;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModelCallback;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListPresenter;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListView;

import java.util.List;

/**
 * Created by marco on 26/12/2016.
 */

public class UserListPresenterImpl implements IUsersListPresenter, IUsersListModelCallback {


    final IUsersListView view;
    final IUsersListModel model;
    final IActivityInterface activityInterface;
    final UsersListDataPreloader preloader;

    boolean isGrid = false;

    /**
     * Used for testing
     */

    public UserListPresenterImpl(IUsersListView view, IUsersListModel model, IActivityInterface activityInterface, UsersListDataPreloader preloader) {
        this.view = view;
        this.model = model;
        this.activityInterface = activityInterface;
        this.preloader = preloader;
    }

    public UserListPresenterImpl(IActivityInterface activityInterface) {
        this.activityInterface = activityInterface;
        this.view = new UsersListViewImpl(this, activityInterface.getActivity());
        this.model = new UsersListModelImpl(this, activityInterface.getActivity());
        this.preloader = new UsersListDataPreloader(this.model);
    }

    @Override
    public void onNewItemsArrived(List<ListItemUser> items) {
        view.insertNewItems(items);
    }

    @Override
    public void onLayoutRestore(boolean isGrid) {
        this.isGrid = isGrid;
        view.changeLayout(isGrid);
    }

    @Override
    public void onNetworkIssueOccured(boolean isHappening) {
        view.notifyNetworkIssue(isHappening);
    }

    @Override
    public void onLoadingInProgress(boolean isHappening) {
        view.notifyLoadingIsInProgress(isHappening);
    }

    @Override
    public void onUserEntryClicked(String userName) {
        activityInterface.startActivity(getIntentForDetailScreen(userName));
    }

    private Intent getIntentForDetailScreen(String username){
        Intent intent = new Intent(activityInterface.getActivity(), UserDetailActivity.class);
        intent.putExtra(UserDetailActivity.ARG_USERNAME, username);
        return intent;
    }

    @Override
    public void onLayoutTypeTriggered() {
        isGrid = !isGrid;
        view.changeLayout(isGrid);
        model.saveLayoutState(isGrid);
    }

    @Override
    public void onNewItemRendered(int position, int count) {
        preloader.notifyItemRendered(position, count);
    }
}
