package com.ostan.feingoldtest.screens.userdetailscreen;

import android.util.Log;

import com.ostan.feingoldtest.pojo.SingleUser;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailModel;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailModelCallback;
import com.ostan.feingoldtest.utils.api.APIConnector;

import rx.Observer;

/**
 * Created by marco on 28/12/2016.
 */

public class UserDetailModelImpl implements IUserDetailModel {

    final IUserDetailModelCallback callback;

    public UserDetailModelImpl(IUserDetailModelCallback callback) {
        this.callback = callback;
    }

    @Override
    public void requestUserDataLoading(String username) {

        new APIConnector().loadUserData(username, new Observer<SingleUser>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                callback.onUserDataArrived(null);
            }

            @Override
            public void onNext(SingleUser singleUser) {
                callback.onUserDataArrived(singleUser);
                Log.i("LOG", "SingleUser Arrived: " + singleUser.getName());
            }
        });
    }
}
