package com.ostan.feingoldtest.screens.userdetailscreen.interfaces;

import com.ostan.feingoldtest.pojo.SingleUser;

/**
 * Created by marco on 28/12/2016.
 */

public interface IUserDetailModelCallback {
    /**
     * Callback with a newly loaded data
     * @param userData - userdata to pass for the presentation
     * */
    public void onUserDataArrived(SingleUser userData);
}
