package com.ostan.feingoldtest.screens.userdetailscreen.interfaces;

/**
 * Created by marco on 28/12/2016.
 */

public interface IUserDetailModel {
    /**
     * Request loading data by username
     * @param username - username that the data should be requested by
     * */
    public void requestUserDataLoading(String username);
}
