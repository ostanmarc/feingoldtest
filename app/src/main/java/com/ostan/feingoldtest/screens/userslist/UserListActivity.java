package com.ostan.feingoldtest.screens.userslist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ostan.feingoldtest.R;
import com.ostan.feingoldtest.screens.userslist.interfaces.IActivityInterface;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListPresenter;


public class UserListActivity extends AppCompatActivity implements IActivityInterface{


    IUsersListPresenter callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        callback = new UserListPresenterImpl(this);
        setupToolBar();


    }

    private void setupToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }


    @Override
    public AppCompatActivity getActivity() {
        return this;
    }
}
