package com.ostan.feingoldtest;

import com.ostan.feingoldtest.preloader.UsersListDataPreloader;
import com.ostan.feingoldtest.screens.userslist.ActivityInterfaceTestImpl;
import com.ostan.feingoldtest.screens.userslist.UserListPresenterImpl;
import com.ostan.feingoldtest.screens.userslist.UsersListModelImpl;
import com.ostan.feingoldtest.screens.userslist.UsersListViewImpl;
import com.ostan.feingoldtest.screens.userslist.interfaces.IActivityInterface;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModel;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListView;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 29/12/2016.
 */

public class UserListPreloaderTest {


    IUsersListView view = mock(UsersListViewImpl.class);
    IUsersListModel model = mock(UsersListModelImpl.class);
    IActivityInterface activity = mock(ActivityInterfaceTestImpl.class);
    UsersListDataPreloader preloader = new UsersListDataPreloader(model, 0.8f, 0.35f);
    UserListPresenterImpl presenter = mock(UserListPresenterImpl.class);


    @Test
    public void testNoReachedPreloadingTrigger(){
        preloader.notifyItemRendered(80, 100);
        verify(model, times(0)).onLoadItemsRequested(100, 35);
    }


    @Test
    public void testTriggerMoreItemsLoading(){

        preloader.notifyItemRendered(81, 100);
        verify(model, times(1)).onLoadItemsRequested(100, 35);

        preloader.notifyItemRendered(100, 135);
        verify(model, times(0)).onLoadItemsRequested(135, 182);
    }

    @Test
    public void testExtremelySensitivePreloader(){
        preloader = new UsersListDataPreloader(model, 0.30f, 0.9f);

        preloader.notifyItemRendered(31, 100);
        verify(model, times(1)).onLoadItemsRequested(100, 90);

    }

    @Test
    public void testExtremelyNONSensitivePreloader(){
        preloader = new UsersListDataPreloader(model, 0.99f, 0.01f);

        preloader.notifyItemRendered(99, 100);
        verify(model, times(0)).onLoadItemsRequested(100, 90);

        preloader.notifyItemRendered(100, 100);
        verify(model, times(1)).onLoadItemsRequested(100, 1);

    }


}
