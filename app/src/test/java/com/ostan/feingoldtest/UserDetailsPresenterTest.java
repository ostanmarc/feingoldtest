package com.ostan.feingoldtest;

import android.support.v7.app.AppCompatActivity;

import com.ostan.feingoldtest.pojo.SingleUser;
import com.ostan.feingoldtest.screens.userdetailscreen.UserDetailModelImpl;
import com.ostan.feingoldtest.screens.userdetailscreen.UserDetailPresenterImpl;
import com.ostan.feingoldtest.screens.userdetailscreen.UserDetailViewImpl;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailModel;
import com.ostan.feingoldtest.screens.userdetailscreen.interfaces.IUserDetailView;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 31/12/2016.
 */

public class UserDetailsPresenterTest {


    IUserDetailView view = mock(UserDetailViewImpl.class);
    IUserDetailModel model = mock(UserDetailModelImpl.class);
    AppCompatActivity activity = mock(AppCompatActivity.class);
    UserDetailPresenterImpl presenter = new UserDetailPresenterImpl(activity, view, model);

    @Test
    public void testUserDataArrivedLogicsValidInput(){
        SingleUser user = new SingleUser();
        presenter.onUserDataArrived(user);
        verify(view, times(1)).setIsLoading(false);
        verify(view, times(1)).setUserData(user);
    }

    @Test
    public void testUserDataArrivedLogicsInvalidInput(){
        SingleUser user = null;
        presenter.onUserDataArrived(user);
        verify(view, times(0)).setIsLoading(false);
        verify(view, times(0)).setUserData(user);
        verify(view, times(1)).notifyUserInputIsInvalid();
    }

    @Test
    public void testHandlingExternallyReceivedInvalidData(){

        String username = null;
        presenter.onUserNameReceived(username);
        verify(view, times(0)).setIsLoading(true);
        verify(model, times(0)).requestUserDataLoading(null);

        username = "";
        presenter.onUserNameReceived(username);
        verify(view, times(0)).setIsLoading(true);
        verify(model, times(0)).requestUserDataLoading(null);

    }

    @Test
    public void testHandlingExternallyReceivedValidData(){
        String username = "testName";
        presenter.onUserNameReceived(username);
        verify(view, times(1)).setIsLoading(true);
        verify(model, times(1)).requestUserDataLoading(username);
    }


}
