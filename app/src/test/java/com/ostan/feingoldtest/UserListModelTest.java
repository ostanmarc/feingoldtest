package com.ostan.feingoldtest;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.screens.userslist.ActivityInterfaceTestImpl;
import com.ostan.feingoldtest.screens.userslist.DataFragment;
import com.ostan.feingoldtest.screens.userslist.UserListPresenterImpl;
import com.ostan.feingoldtest.screens.userslist.UsersListModelImpl;
import com.ostan.feingoldtest.utils.api.APIConnector;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 30/12/2016.
 */

public class UserListModelTest {
    DataFragment dataFragment = mock(DataFragment.class);
    ActivityInterfaceTestImpl activity = mock(ActivityInterfaceTestImpl.class);
    UserListPresenterImpl presenter = mock(UserListPresenterImpl.class);
    APIConnector connector = mock(APIConnector.class);
    UsersListModelImpl model = new UsersListModelImpl(presenter,activity.getActivity(),dataFragment, connector);

    @Test
    public void testLayoutStateSaving(){
        boolean isGrid = true;
        model.saveLayoutState(isGrid);
        verify(dataFragment, times(1)).setGrid(isGrid);

        isGrid = false;
        model.saveLayoutState(isGrid);
        verify(dataFragment, times(1)).setGrid(isGrid);

    }

    @Test
    public void testEmptyOrNullDataReceivedFiltering(){
        model.onNext(new ArrayList<ListItemUser>());
        verify(presenter, times(0)).onNewItemsArrived(null);

        model.onNext(null);
        verify(presenter, times(0)).onNewItemsArrived(null);

    }

    @Test
    public void testNonEmptyDataReceivedFiltering(){
        List<ListItemUser> data = new ArrayList<>();
        data.add(new ListItemUser());
        model.onNext(data);
        verify(presenter, times(1)).onNewItemsArrived(data);
        model.onCompleted();
    }

    @Test
    public void testItemsLoadingMechanism(){

        // check new request
        model.onLoadItemsRequested(1, 30);
        verify(connector, times(1)).getAllUsersReactive(1, 30, model);

        // check second new request not sent to server
        model.onLoadItemsRequested(1, 30);
        verify(connector, times(1)).getAllUsersReactive(1, 30, model);

        // set request finished
        model.onCompleted();

        // verify that next request can be sent now
        model.onLoadItemsRequested(1, 30);
        verify(connector, times(2)).getAllUsersReactive(1, 30, model);

        // set request finished with error
        model.onError(null);

        // verify that next request can be sent now
        model.onLoadItemsRequested(1, 30);
        verify(connector, times(3)).getAllUsersReactive(1, 30, model);


    }
}
