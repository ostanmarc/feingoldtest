package com.ostan.feingoldtest;

import com.ostan.feingoldtest.pojo.ListItemUser;
import com.ostan.feingoldtest.preloader.UsersListDataPreloader;
import com.ostan.feingoldtest.screens.userslist.ActivityInterfaceTestImpl;
import com.ostan.feingoldtest.screens.userslist.UserListPresenterImpl;
import com.ostan.feingoldtest.screens.userslist.UsersListModelImpl;
import com.ostan.feingoldtest.screens.userslist.UsersListViewImpl;
import com.ostan.feingoldtest.screens.userslist.interfaces.IActivityInterface;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListModel;
import com.ostan.feingoldtest.screens.userslist.interfaces.IUsersListView;

import org.junit.Test;

import java.util.ArrayList;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by marco on 29/12/2016.
 */

public class UserListPresenterTest {


    IUsersListView view = mock(UsersListViewImpl.class);
    IUsersListModel model = mock(UsersListModelImpl.class);
    IActivityInterface activity = mock(ActivityInterfaceTestImpl.class);
    UsersListDataPreloader preloader = mock(UsersListDataPreloader.class);
    UserListPresenterImpl presenter = new UserListPresenterImpl(view, model, activity, preloader);

    @Test
    public void testNewItemArrival() {

        presenter.onNewItemsArrived(new ArrayList<ListItemUser>());
        verify(view, times(1)).insertNewItems(new ArrayList<ListItemUser>());

    }

    @Test
    public void testLayoutExchange() {

        presenter.onLayoutTypeTriggered();
        verify(view, times(1)).changeLayout(true);
        verify(model, times(1)).saveLayoutState(true);

        presenter.onLayoutTypeTriggered();
        verify(view, times(1)).changeLayout(false);
        verify(model, times(1)).saveLayoutState(false);

    }

    @Test
    public void testNewItemRenderingLogics() {
        presenter.onNewItemRendered(0, 1);
        verify(preloader, times(1)).notifyItemRendered(0, 1);
    }

    @Test
    public void testOnLayoutRestore(){
     presenter.onLayoutRestore(true);
        verify(view, times(1)).changeLayout(true);
    }


}
